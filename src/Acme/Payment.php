<?php

namespace Acme;


class Payment
{
    private $customerEmail;
    private $customerPhone;
    private $customerFirstName;
    private $customerLastName;
    private $shipmentStreet;
    private $shipmentPostalCode;
    private $shipmentCity;
    private $shipmentRecipentName;

    /**
     * @return mixed
     */
    public function getCustomerEmail()
    {
        return $this->customerEmail;
    }

    /**
     * @param mixed $customerEmail
     */
    public function setCustomerEmail($customerEmail)
    {
        $this->customerEmail = $customerEmail;
    }

    /**
     * @return mixed
     */
    public function getCustomerPhone()
    {
        return $this->customerPhone;
    }

    /**
     * @param mixed $customerPhone
     */
    public function setCustomerPhone($customerPhone)
    {
        $this->customerPhone = $customerPhone;
    }

    /**
     * @return mixed
     */
    public function getCustomerFirstName()
    {
        return $this->customerFirstName;
    }

    /**
     * @param mixed $customerFirstName
     */
    public function setCustomerFirstName($customerFirstName)
    {
        $this->customerFirstName = $customerFirstName;
    }

    /**
     * @return mixed
     */
    public function getCustomerLastName()
    {
        return $this->customerLastName;
    }

    /**
     * @param mixed $customerLastName
     */
    public function setCustomerLastName($customerLastName)
    {
        $this->customerLastName = $customerLastName;
    }

    /**
     * @return mixed
     */
    public function getShipmentStreet()
    {
        return $this->shipmentStreet;
    }

    /**
     * @param mixed $shipmentStreet
     */
    public function setShipmentStreet($shipmentStreet)
    {
        $this->shipmentStreet = $shipmentStreet;
    }

    /**
     * @return mixed
     */
    public function getShipmentPostalCode()
    {
        return $this->shipmentPostalCode;
    }

    /**
     * @param mixed $shipmentPostalCode
     */
    public function setShipmentPostalCode($shipmentPostalCode)
    {
        $this->shipmentPostalCode = $shipmentPostalCode;
    }

    /**
     * @return mixed
     */
    public function getShipmentCity()
    {
        return $this->shipmentCity;
    }

    /**
     * @param mixed $shipmentCity
     */
    public function setShipmentCity($shipmentCity)
    {
        $this->shipmentCity = $shipmentCity;
    }

    /**
     * @return mixed
     */
    public function getShipmentRecipentName()
    {
        return $this->shipmentRecipentName;
    }

    /**
     * @param mixed $shipmentRecipentName
     */
    public function setShipmentRecipentName($shipmentRecipentName)
    {
        $this->shipmentRecipentName = $shipmentRecipentName;
    }
}