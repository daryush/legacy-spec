<?php

namespace Calculator;

use Psr\Log\LoggerInterface;

class Calculator
{
    /**
     * @var Operation
     */
    private $add;
    /**
     * @var Operation
     */
    private $divide;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(Operation $add, Operation $divide, LoggerInterface $logger)
    {
        $this->add = $add;
        $this->divide = $divide;
        $this->logger = $logger;
    }

    public function add($numberOne, $numberTwo)
    {
        return $this->add->calculate($numberOne, $numberTwo);
    }

    public function divide($numberOne, $numberTwo)
    {
        if ($numberTwo === 0) {
            $this->logger->error("Division by zero!");

            throw new \LogicException('Dont divide by zero!');
        }

        return $this->divide->calculate($numberOne, $numberTwo);
    }
}
