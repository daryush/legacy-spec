<?php

namespace Calculator;

interface Operation
{

    public function calculate($argument1, $argument2);
}
