<?php

namespace spec\Calculator;

use Calculator\Calculator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Calculator\Operation;
use Psr\Log\LoggerInterface;

class CalculatorSpec extends ObjectBehavior
{
    function let(Operation $add, Operation $divide, LoggerInterface $logger)
    {
        $this->beConstructedWith($add, $divide, $logger);
    }

    function it_adds_two_numbers(Operation $add)
    {
        $add->calculate(2, 1)->willReturn(3);
        $add->calculate(4, 1)->willReturn(5);

        $this->add(2, 1)->shouldReturn(3);
        $this->add(4, 1)->shouldReturn(5);
    }

    function it_divides_two_numbers(Operation $divide)
    {
        $divide->calculate(2, 1)->willReturn(2);
        $divide->calculate(6, 2)->willReturn(3);

        $this->divide(2, 1)->shouldReturn(2);
        $this->divide(6, 2)->shouldReturn(3);
    }

    function it_do_not_allow_to_divide_by_zero()
    {
        $this->shouldThrow(new \LogicException('Dont divide by zero!'))
            ->duringDivide(Argument::type('int'), 0);
    }

    function it_logs_divisions_by_zero(LoggerInterface $logger)
    {
        $logger->error('Division by zero!')->shouldBeCalled();

        $this->shouldThrow(new \LogicException('Dont divide by zero!'))
            ->duringDivide(Argument::type('int'), 0);
    }
}
